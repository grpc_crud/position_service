CREATE TABLE IF NOT EXISTS profession ( 
    id uuid primary key, 
    name varchar(255) not null 
); 

CREATE TABLE IF NOT EXISTS attribute ( 
    id uuid primary key, 
    name varchar(255) not null ,
    attribute_type varchar(255)
);

