CREATE TABLE IF NOT EXISTS position ( 
    id uuid primary key, 
    name varchar(255) not null,
    profession_id uuid,
    company_id uuid not null,
    constraint fk_profession foreign key("profession_id") references profession ("id")
); 

CREATE TABLE IF NOT EXISTS position_attributes ( 
    id uuid primary key, 
    value varchar(255) not null,
    attribute_id uuid references attribute(id), 
    position_id uuid references position(id)
); 
