package service

import (
	"context"

	"bitbucket.org/Udevs/position_service/genproto/position_service"
	"bitbucket.org/Udevs/position_service/pkg/helper"
	"bitbucket.org/Udevs/position_service/pkg/logger"
	"bitbucket.org/Udevs/position_service/storage"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/emptypb"
)

type attributeService struct {
	logger  logger.Logger
	storage storage.StrogeI
}

func NewAttributeService(log logger.Logger, db *sqlx.DB) *attributeService {
	return &attributeService{
		logger:  log,
		storage: storage.NewStoragePG(db),
	}
}

func (s *attributeService) Create(ctx context.Context, req *position_service.CreateAttribute) (*position_service.AttributeId, error) {
	id, err := s.storage.Attrubite().Create(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while attribute", req, codes.Internal)
	}

	return &position_service.AttributeId{
		Id: id,
	}, nil
}

func (s *attributeService) Get(ctx context.Context, req *position_service.AttributeId) (*position_service.Attribute, error) {
	attribute, err := s.storage.Attrubite().Get(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting attribute ", req, codes.Internal)
	}

	return attribute, nil
}

func (s *attributeService) GetAll(ctx context.Context, req *position_service.GetAllAttributeRequest) (*position_service.GetAllAttributeResponse, error) {
	attribute, err := s.storage.Attrubite().GetAll(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting all attributes ", req, codes.Internal)
	}

	return attribute, nil
}

func (s *attributeService) Update(ctx context.Context, req *position_service.Attribute) (*position_service.Attribute, error) {
	profession, err := s.storage.Attrubite().Update(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while  updating attribute ", req, codes.Internal)
	}

	return profession, nil
}

func (s *attributeService) Delete(ctx context.Context, req *position_service.AttributeId) (*emptypb.Empty, error) {
	err := s.storage.Attrubite().Delete(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while deleting profession ", req, codes.Internal)
	}

	return &emptypb.Empty{}, nil
}
