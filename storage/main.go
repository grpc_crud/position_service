package storage

import (
	"bitbucket.org/Udevs/position_service/storage/postgres"
	"bitbucket.org/Udevs/position_service/storage/repo"
	"github.com/jmoiron/sqlx"
)

type StrogeI interface {
	Profession() repo.ProfessionRepoI
	Attrubite() repo.AttributeRepoI
	Position() repo.PositionRepoI
}

type storagePG struct {
	profession repo.ProfessionRepoI
	attribute  repo.AttributeRepoI
	position   repo.PositionRepoI
}

func NewStoragePG(db *sqlx.DB) StrogeI {
	return &storagePG{
		profession: postgres.NewProfessionRepo(db),
		attribute:  postgres.NewAttributeRepo(db),
		position:   postgres.NewPositionRepo(db),
	}
}

func (s *storagePG) Profession() repo.ProfessionRepoI {
	return s.profession
}

func (s *storagePG) Attrubite() repo.AttributeRepoI {
	return s.attribute
}

func (s *storagePG) Position() repo.PositionRepoI {
	return s.position
}
