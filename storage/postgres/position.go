package postgres

import (
	"fmt"

	"bitbucket.org/Udevs/position_service/genproto/position_service"
	"bitbucket.org/Udevs/position_service/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type positionRepo struct {
	db *sqlx.DB
}

func NewPositionRepo(db *sqlx.DB) repo.PositionRepoI {
	return &positionRepo{db: db}
}

func (r *positionRepo) Create(req *position_service.CreatePosition) (string, error) {
	var (
		position_id uuid.UUID
		pos_att_id  uuid.UUID
	)
	tx, err := r.db.Begin()

	if err != nil {
		return "", err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	position_id, err = uuid.NewRandom()
	if err != nil {
		return "", err
	}

	query := ` INSERT INTO position (id, name, profession_id, company_id ) VALUES($1, $2, $3, $4) `

	_, err = tx.Exec(query, position_id, req.Name, req.ProfessionId, req.CompanyId)
	if err != nil {
		fmt.Println("error message", err.Error())
		return "", err
	}

	for _, v := range req.PositionAttributes {
		pos_att_query := ` INSERT INTO position_attributes (id, value, attribute_id, position_id) VALUES($1, $2, $3, $4) `

		pos_att_id, err = uuid.NewRandom()
		if err != nil {
			return "", err
		}

		_, err = tx.Exec(pos_att_query, pos_att_id, v.Value, v.AttributeId, position_id)
		if err != nil {
			return "", err
		}
	}

	return position_id.String(), nil
}

func (r *positionRepo) Get(id string) (*position_service.Position, error) {
	var position position_service.Position

	queryPosition := `
		SELECT id, name, profession_id, company_id
		FROM position
		WHERE id = $1
	`

	row := r.db.QueryRow(queryPosition, id)
	err := row.Scan(
		&position.Id,
		&position.Name,
		&position.ProfesssionId,
		&position.CompanyId,
	)

	if err != nil {
		return nil, err
	}

	queryPositionAttributeJoinAtribute := ` SELECT 
												pa.id,
												pa.value,
												pa.attribute_id,
												pa.position_id,
												a.id,
												a.name, 
												a.attribute_type 
											FROM position_attributes pa 
											  JOIN attribute a on pa.attribute_id = a.id
												WHERE position_id = $1 `

	rows, err := r.db.Query(queryPositionAttributeJoinAtribute, id)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var position_attribute position_service.GetPositionAttribute
		var attribute position_service.Attribute

		err := rows.Scan(
			&position_attribute.Id,
			&position_attribute.Value,
			&position_attribute.AttributeId,
			&position_attribute.PositionId,
			&attribute.Id,
			&attribute.Name,
			&attribute.AttributeType,
		)
		if err != nil {
			return nil, err
		}
		position_attribute.Attribute = &attribute
		position.PositionAttributes = append(position.PositionAttributes, &position_attribute)
	}

	return &position, nil
}

func (r *positionRepo) GetAll(req *position_service.GetAllPositionRequest) (*position_service.GetAllPositionResponse, error) {

	var args = make(map[string]interface{})
	var positions []*position_service.Position
	var filter string
	var count uint32

	if req.Name != "" {
		filter += ` AND name ILIKE '%' || :name || '%' `
		args["name"] = req.Name
	}

	if req.CompanyId != "" {
		filter += ` AND company_id = :company_id `
		args["company_id"] = req.CompanyId
	}

	if req.ProfessionId != "" {
		filter += ` AND profession_id = :profession_id `
		args["profession_id"] = req.ProfessionId
	}

	countQuery := `
		SELECT count(1)
		FROM position
		WHERE true
		` + filter

	rows, err := r.db.NamedQuery(countQuery, args)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Scan(
			&count,
		)

		if err != nil {
			return nil, err
		}
	}

	filter += `OFFSET :offset LIMIT :limit`
	args["limit"] = req.Limit
	args["offset"] = req.Offset

	queryPosition := `
			SELECT id, name, profession_id, company_id
			FROM position
			WHERE true
		` + filter

	rows, err = r.db.NamedQuery(queryPosition, args)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var position position_service.Position

		err := rows.Scan(
			&position.Id,
			&position.Name,
			&position.ProfesssionId,
			&position.CompanyId,
		)

		if err != nil {
			return nil, err
		}

		queryPositionAttributeJoinAtribute := ` SELECT 
												      pa.id,
												      pa.value,
												      pa.attribute_id,
												      pa.position_id,
												      a.id,
												      a.name, 
												      a.attribute_type 
												FROM position_attributes pa 
												  JOIN attribute a on pa.attribute_id = a.id
												WHERE position_id = $1 `

		rows, err := r.db.Query(queryPositionAttributeJoinAtribute, position.Id)

		if err != nil {
			return nil, err
		}

		for rows.Next() {
			var position_attribute position_service.GetPositionAttribute
			var attribute position_service.Attribute

			err := rows.Scan(
				&position_attribute.Id,
				&position_attribute.Value,
				&position_attribute.AttributeId,
				&position_attribute.PositionId,
				&attribute.Id,
				&attribute.Name,
				&attribute.AttributeType,
			)
			if err != nil {
				return nil, err
			}
			position_attribute.Attribute = &attribute
			position.PositionAttributes = append(position.PositionAttributes, &position_attribute)
		}

		positions = append(positions, &position)
	}

	return &position_service.GetAllPositionResponse{
		Positions: positions,
		Count:     count,
	}, nil
}

func (r *positionRepo) Update(req *position_service.UpdatePosition) (*position_service.MessageRes, error) {

	tx, err := r.db.Begin()

	if err != nil {
		return &position_service.MessageRes{
			Id: "",
		}, err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	queryPositition := `
		UPDATE position
		SET 
			name = $1, 
			profession_id = $2, 
			company_id = $3
		WHERE id = $4 `

	_, err = tx.Exec(queryPositition, req.Name, req.ProfessionId, req.CompanyId, req.Id)
	if err != nil {
		return &position_service.MessageRes{
			Id: "",
		}, err
	}


	delete := ` DELETE FROM position_attributes
	WHERE position_id = $1 `

	_, err = tx.Exec(delete, req.Id)
	if err != nil {
		return &position_service.MessageRes{
			Id: "Error!! Not Deleted",
		}, err
	}
	var pos_att_id uuid.UUID
	queryPositionAttribute := ` INSERT INTO position_attributes (id, value, attribute_id, position_id) VALUES ($1, $2, $3, $4) `

	for _, val := range req.PositionAttributes {
		pos_att_id, err = uuid.NewRandom()
		
		_, err = tx.Exec(queryPositionAttribute, pos_att_id, val.Value, val.AttributeId, req.Id)
		if err != nil {
			return &position_service.MessageRes{
				Id: "",
			}, err
		}
	}
	return &position_service.MessageRes{
		Id: "Successfully Updated",
	}, nil

}

func (r *positionRepo) Delete(id string) (*position_service.MessageRes, error) {
	tx, err := r.db.Begin()

	if err != nil {
		return &position_service.MessageRes{
			Id: "",
		}, err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	queryPositionAttribute := `
		DELETE FROM position_attributes
		WHERE position_id = $1
	`

	_, err = tx.Exec(queryPositionAttribute, id)

	if err != nil {
		return &position_service.MessageRes{
			Id: "Not Deleted",
		}, err
	}

	queryPositition := `
		DELETE FROM position
		WHERE id = $1
	`

	_, err = tx.Exec(queryPositition, id)

	if err != nil {
		return &position_service.MessageRes{
			Id: "Error!! Not Deleted",
		}, err
	}

	return &position_service.MessageRes{
		Id: "Successfully Deleted",
	}, err
}
